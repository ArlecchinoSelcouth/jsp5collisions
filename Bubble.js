function Bubble(x, y, r) {
    this.x = x;
    this.y = y;
    this.r = r;
    this.colour = color(255, 100);
    this.bias = 1;

    this.display = function() {
        stroke(255);
        fill(this.colour);
        ellipse(this.x, this.y, this.r * 2, this.r * 2);
    }

    this.update = function() {
        let jump = this.bias * 3;
        // let jump  = 1;
        this.x += random(-jump, jump);
        this.y += random(-jump, jump);
    }

    this.changeColor = function() {
        this.colour = color(150, 0, 0);
        this.contaminated = true;
    }

    this.intersects = function(obj) {
        var center_distance = dist(this.x, this.y, obj.x, obj.y);
        if(center_distance < this.r + obj.r) {
            return true;
        } else {
            return false;
        }
    }

    this.resetColor = function() {
        this.colour = color(255, 100);
    }
}