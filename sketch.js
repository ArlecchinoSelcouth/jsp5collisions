var bubbles = [];

function setup() {
  createCanvas(900, 900);
  for (let i = 0; i < 200; i++) {
    bubbles[i] = new Bubble(random(width), random(height), 10);
  }
}

function draw() {
  background(0);

  for (bubble of bubbles) {
    bubble.update();
    bubble.display();
    let inContact = false;
    for (another_bubble of bubbles) {
      if(another_bubble != bubble) {
        if(bubble.intersects(another_bubble)) {
          bubble.changeColor();
          another_bubble.changeColor();
          inContact = true;
        }
      }
    }
    if(!inContact) {
      bubble.resetColor();
    }
  }
}
